<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Welcome extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $u = new User();
        $records = $u->get();
        print_r($records);
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
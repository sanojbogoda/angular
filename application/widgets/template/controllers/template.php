<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Template extends MX_Controller {

    public function common($data = '') {
        $this->load->view('common_view', $data);
    }

    public function emp($data = '') {
       $this->load->view('empty_view', $data);  
    }

}

/*------------------------------------------------------------------------------*/
/*                              End of the class                                */
/*------------------------------------------------------------------------------*/ 

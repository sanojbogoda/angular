<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Safe Zone | Guerrilla</title>
        <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" /> 
        
        <?php $this->carabiner->display('todo', 'css');?>
        
        <!--[if lt IE 9]>
        <?php $this->carabiner->display('IE','js');?>
        <![endif]-->
        
        <!--Sanoj's additional styles-->
        <style type="text/css">
            
            .loader{
                display: none;
            }
            
        </style>
        
    </head>
    <body>
        <section class="vbox">
            <header class="header bg-black navbar navbar-inverse pull-in">
                <div class="navbar-header nav-bar aside dk">
                    <a class="btn btn-link visible-xs" data-toggle="class:show" data-target=".nav-primary">
                        <i class="icon-reorder"></i>
                    </a>
                    <a href="#" class="nav-brand" data-toggle="fullscreen">SafeZone</a>
                    <a class="btn btn-link visible-xs" data-toggle="collapse" data-target=".navbar-collapse">
                        <i class="icon-comment-alt"></i>
                    </a>
                </div>
                <div class="collapse navbar-collapse">
                    <form class="navbar-form navbar-left m-t-sm" role="search">
                        <div class="form-group">
                            <div class="input-group input-s">
                                <input type="text" class="form-control input-sm no-border dk text-white" placeholder="Search">
                                <span class="input-group-btn">
                                    <button type="submit" class="btn btn-sm btn-primary btn-icon"><i class="icon-search"></i></button>
                                </span>
                            </div>
                        </div>
                    </form>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="hidden-xs">
                            <a href="#" class="dropdown-toggle dk" data-toggle="dropdown">
                                <i class="icon-bell-alt text-white"></i>
                                <span class="badge up bg-danger m-l-n-sm">2</span>
                            </a>
                            <section class="dropdown-menu animated fadeInUp input-s-lg">
                                <section class="panel bg-white">
                                    <header class="panel-heading">
                                        <strong>You have <span class="count-n">2</span> notifications</strong>
                                    </header>
                                    <div class="list-group">
                                        <a href="#" class="media list-group-item">
                                            <span class="pull-left thumb-sm">
                                                <img src="<?php echo base_url();?>assets/images/avatar.jpg" alt="John said" class="img-circle">
                                            </span>
                                            <span class="media-body block m-b-none">
                                                Use awesome animate.css<br>
                                                <small class="text-muted">28 Aug 13</small>
                                            </span>
                                        </a>
                                        <a href="#" class="media list-group-item">
                                            <span class="media-body block m-b-none">
                                                1.0 initial released<br>
                                                <small class="text-muted">27 Aug 13</small>
                                            </span>
                                        </a>
                                    </div>
                                    <footer class="panel-footer text-sm">
                                        <a href="#" class="pull-right"><i class="icon-cog"></i></a>
                                        <a href="#">See all the notifications</a>
                                    </footer>
                                </section>
                            </section>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle aside-sm dker" data-toggle="dropdown">
                                <span class="thumb-sm avatar pull-left m-t-n-xs m-r-xs">
                                    <img src="<?php echo base_url();?>assets/images/avatar.jpg">
                                </span>
                                 John Doe <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu animated fadeInLeft">
                                <li>
                                    <a href="#">Settings</a>
                                </li>
                                <li>
                                    <a href="profile.html">Profile</a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="badge bg-danger pull-right">3</span>
                                        Notifications
                                    </a>
                                </li>
                                <li>
                                    <a href="docs.html">Help</a>
                                </li>
                                <li>
                                    <a href="signin.html">Logout</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </header>
            <section>
                <section class="hbox stretch">
                    <aside class="aside bg-light lter b-r" id="nav">
                        <nav class="nav-primary hidden-xs">
                            <ul class="nav">
                                <li>
                                    <a href="<?php echo base_url().index_page().'home';?>">
                                        <i class="icon-home"></i>
                                        <span>Home</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url().index_page().'project';?>">
                                        <i class="icon-copy"></i>
                                        <span>Projects</span>
                                    </a>
                                </li>
                                <li class="dropdown-submenu">
                                    <a href="<?php echo base_url().index_page().'contractor/main';?>">
                                        <i class="icon-building"></i>
                                        <span>Main-Contractor</span>
                                    </a>
                                </li>
                                <li class="dropdown-submenu">
                                    <a href="<?php echo base_url().index_page().'user';?>">
                                        <i class="icon-group"></i>
                                        <span>Staff</span>
                                    </a>
                                </li>
                                <li class="dropdown-submenu">
                                    <a href="<?php echo base_url().index_page().'contractor';?>">
                                        <i class="icon-building"></i>
                                        <span>Sub-Contractors</span>
                                    </a>
                                </li>
                                <li class="dropdown-submenu">
                                    <a href="<?php echo base_url().index_page().'offence/type';?>">
                                        <i class="icon-flag"></i>
                                        <span>Offense Types</span>
                                    </a>
                                </li>
                                <li class="dropdown-submenu">
                                    <a href="<?php echo base_url().index_page().'offence';?>">
                                        <i class="icon-flag-alt"></i>
                                        <span>Offenses</span>
                                    </a>
                                </li>
                                <li class="dropdown-submenu">
                                    <a href="<?php echo base_url().index_page().'profile';?>">
                                        <i class="icon-smile"></i>
                                        <span>My Profile</span>
                                    </a>
                                </li>
                                
                            </ul>
                        </nav>
                    </aside>
                    <section>
                        <div class="wrapper">
                            
                            <?php $this->load->view($this->router->fetch_class() . '/' . $view);?>
                            
                        </div>
                    </section>
<!--                    <aside class="bg-light lter b-l aside-sm">
                        <div class="wrapper">aside content</div>
                    </aside>-->
                </section>
            </section>
<!--            <footer class="footer bg-white b-t">
                <p>2013 &COPY; Powered by Guerrilla Media Pte Ltd, A company registered in Singapore.</p>
            </footer>-->
        </section>

    <?php $this->carabiner->display('todo','js');?>
        
        <!--Sanoj's additional scripts-->
        <script type="text/javascript">
        
            $(document).ready(function(){
                
                try{ 
                    //Run after page load
                    init();
                    //datapicker
                    $('.datepicker').datepicker({
                        'format':'yyyy-mm-dd'
                    }).on('changeDate', function(){
                        $('div.datepicker').hide();
                    }); 
                    
                    $(".delete").click(function () {
                        if (confirm('Are you sure to delete?')) {
                            return true;
                        }
                        return false;
                    });
                }
                catch(e){
                    //alert(e);
                }
                
            });
        
        </script>
        
    </body>
</html>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <title>AngularJS | Sanoj</title>
        <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" /> 
        <?php $this->carabiner->display('todo', 'css'); ?>
        <!--[if lt IE 9]>
        <?php $this->carabiner->display('IE', 'js'); ?>
        <![endif]-->
    </head>
    <body>
        
        <section class="vbox">

            <div ng-app="app" ng-controller="myController">
                
               <h5>Base template</h5>
                <p>
                    Top nav here
                    <a href="<?php echo base_url().index_page();?>foo/#/fooview">Foo view</a>
                </p>
                <hr />
                
                <div ng-view=""></div>
                
                <hr />
                <p>footer</p>

            </div>

        </section>

        <?php $this->carabiner->display('todo', 'js'); ?>
        
        <script type="text/javascript">
            
            var base_url = 'http://localhost/angular/index.php/';
            
            var app = angular.module('app', []);
            
            app.controller('myController', function($scope){
                $scope.people = [{'name':'sanoj','country':'Sri Lanka'},
                    {'name':'Thomas','country':'USA'},
                    {'name':'Oliver','country':'England'}];
            });
            
            app.config(['$routeProvider', function($routeProvider) {
                $routeProvider.when('/view1', {templateUrl: base_url + 'test/view1', controller: MyCtrl1});
                $routeProvider.when('/view2', {templateUrl: base_url + 'test/view2', controller: MyCtrl2});
                $routeProvider.when('/fooview', {templateUrl: base_url + 'foo/view2', controller: MyCtrl2});
                $routeProvider.otherwise({redirectTo: '/view1'});
            }]);


            function MyCtrl1($scope, $http) {

            }
            
            function MyCtrl2($scope, $http) {

            }


        </script>

    </body>
</html>
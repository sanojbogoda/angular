<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Home extends MX_Controller {

    public function index() {

        $data['view'] = 'home_view';
        $data['page'] = 'home';
        $this->load->module('template');
        $this->template->common($data);
    }
    
    public function run() {
        $u = new User();
        $records = $u->get();
        echo '<pre>';
        print_r($records);
    }

}

/*------------------------------------------------------------------------------*/
/*                              End of the class                                */
/*------------------------------------------------------------------------------*/ 

<div class="row">

    <div class="col-md-6">
        <section class="panel clearfix">
            <div class="panel-body">
                <a href="#" class="thumb pull-left m-r">
                    <img src="<?php echo base_url(); ?>assets/images/avatar.jpg" class="img-circle">
                </a>
                <div class="clear">
                    <a href="#" class="text-info">@Mike Mcalidek <i class="icon-twitter"></i></a>
                    <small class="block text-muted">2,415 followers / 225 tweets</small>
                    <a href="#" class="btn btn-xs btn-success m-t-xs">Follow</a>
                </div>
            </div>
        </section>
    </div>

    <div class="col-md-6">

        <section class="panel">
            <div class="panel-body">
                <div class="clearfix m-b">
                    <small class="text-muted pull-right">1hr ago</small>
                    <a href="#" class="thumb-sm pull-left m-r">
                        <img src="<?php echo base_url(); ?>assets/images/avatar_default.jpg" class="img-circle">
                    </a>
                    <div class="clear">
                        <a href="#"><strong>Mike Mcalidek</strong></a>
                        <small class="block text-muted">Newyork, USA</small>
                    </div>
                </div>
                <div class="pull-in bg-light clearfix m-b-n">
                    <p class="m-t-sm m-b text-center animated bounceInDown">
                        <i class="icon-map-marker text-danger icon-4x" data-toggle="tooltip" title="" data-original-title="checked in at Newyork"></i>
                    </p>
                </div>
            </div>
            <footer class="panel-footer pos-rlt">
                <span class="arrow top"></span>
                <form class="pull-out">
                    <input type="text" class="form-control no-border input-lg text-sm" placeholder="Write a comment...">
                </form>
            </footer>
        </section>
    </div>

</div>


<section class="panel">
    <header class="panel-heading font-bold">Site statistics</header>
    <div class="panel-body">
        <div id="flot-color" style="height: 250px; padding: 0px; position: relative;"><canvas class="flot-base" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 778px; height: 250px;" width="778" height="250"></canvas><div class="flot-text" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; font-size: smaller; color: rgb(84, 84, 84);"><div class="flot-x-axis flot-x1-axis xAxis x1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;"><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 64px; top: 235px; left: 14px; text-align: center;">0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 64px; top: 235px; left: 82px; text-align: center;">1</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 64px; top: 235px; left: 150px; text-align: center;">2</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 64px; top: 235px; left: 218px; text-align: center;">3</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 64px; top: 235px; left: 286px; text-align: center;">4</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 64px; top: 235px; left: 354px; text-align: center;">5</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 64px; top: 235px; left: 423px; text-align: center;">6</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 64px; top: 235px; left: 491px; text-align: center;">7</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 64px; top: 235px; left: 559px; text-align: center;">8</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 64px; top: 235px; left: 627px; text-align: center;">9</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 64px; top: 235px; left: 692px; text-align: center;">10</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 64px; top: 235px; left: 760px; text-align: center;">11</div></div><div class="flot-y-axis flot-y1-axis yAxis y1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;"><div class="flot-tick-label tickLabel" style="position: absolute; top: 223px; left: 6px; text-align: right;">0</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 179px; left: 6px; text-align: right;">5</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 135px; left: 0px; text-align: right;">10</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 92px; left: 0px; text-align: right;">15</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 48px; left: 0px; text-align: right;">20</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 5px; left: 0px; text-align: right;">25</div></div></div><canvas class="flot-overlay" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 778px; height: 250px;" width="778" height="250"></canvas></div>
    </div>
    <footer class="panel-footer">
        <div class="row text-center">
            <div class="col-xs-3 b-r">
                <p class="h3 font-bold m-t">5,860</p>
                <p class="text-muted">Orders</p>
            </div>
            <div class="col-xs-3 b-r">
                <p class="h3 font-bold m-t">10,450</p>
                <p class="text-muted">Selling Items</p>
            </div>
            <div class="col-xs-3 b-r">
                <p class="h3 font-bold m-t">21,230</p>
                <p class="text-muted">Items</p>
            </div>
            <div class="col-xs-3">
                <p class="h3 font-bold m-t">7,230</p>
                <p class="text-muted">Customers</p>                        
            </div>
        </div>
    </footer>
</section>





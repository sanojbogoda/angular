<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class API extends REST_Controller {

    public function __construct() {
        parent::__construct();

        //Models

    }

    
    public function index_get() {
        $this->response('Inside the index method', 200);
    }

    
    public function leaves_get($user_id = 0) {
        //Validate
        if ($user_id == 0) {
            $this->response(array('error' => '$user_id is missing'), 400);
        }

        //Database 


        
        if ($employers) {
            $this->response(array($employers,$reasons, $types), 200);
        } else {
            $this->response(array('error' => 'Couldn\'t find any records!'), 404);
        }
        
        
        
    }

    
    public function leave_post() {


        if ($this->post('kind') == 'Full Day') {

            //Get related perameters 


            //Output
            if ($insert) {
                $this->response(array('status' => 'Success'));
            } else {
                $this->response(array('status' => 'Failed'));
            }
        }
    }

    public function leave_pull() {
        
    }

    public function leave_delete() {
        
    }

}

/*------------------------------------------------------------------------------*/
/*                              End of the class                                */
/*------------------------------------------------------------------------------*/ 
<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Project extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Default view
     */
    public function index() {

        $p = new Project_Model();
        $records = $p->get();

        $data['records'] = $records;
        $data['view'] = 'project_view';
        $data['page'] = 'project';
        $this->load->module('template');
        $this->template->common($data);
    }

    /**
     * Get details
     * @param type $id
     */
    public function project_details($id = '') {

        if (empty($id))show_404();

        $p = new Project_Model();
        $found = $p->where('id', $id)->count();
        if (empty($found))
            show_404();
        $p->where('id', $id);
        $records = $p->get();

        $data['row'] = $records;
        $data['view'] = 'project_detail_view';
        $data['page'] = 'project';
        $this->load->module('template');
        $this->template->common($data);
    }

    /**
     * project_modify view add/edit
     * @param type $id
     */
    public function project_modify($id = '') {
        $p = '';
        if (empty($id)) {
            //Add view
            $title = 'Add Project';
            //Add
            $this->add_project();
        } else {
            //Edit view
            $title = 'Edit Project';

            //record exist
            $p = new Project_Model();
            $found = $p->where('id', $id)->count();
            if (empty($found))show_404();

            //Edit
            $this->edit_project($id);

            //Get
            $p->where('id', $id);
            $p->get();
        }

        $data['row'] = $p;
        $data['title'] = $title;
        $data['view'] = 'add_view';
        $data['page'] = 'project';
        $this->load->module('template');
        $this->template->common($data);
    }

    /**
     * Add project
     */
    public function add_project() {

        if ($this->input->post()) {
            $msg = '';
            $p = new Project_Model();
            $p->contract_no = $this->input->post('contract_no');
            $p->pro_name = $this->input->post('pro_name');
            $p->pro_desc = $this->input->post('pro_desc');
            $p->type = $this->input->post('type');
            $p->start_date = $this->input->post('start_date');
            $p->end_date = $this->input->post('end_date');
            $p->site_name = $this->input->post('site_name');
            $p->site_desc = $this->input->post('site_desc');
            $p->site_add = $this->input->post('site_add');
            $p->lat = $this->input->post('lat');
            $p->lon = $this->input->post('lon');
            //add
            $msg = ($p->save()) ? 'Success: Project has been added.' : 'Error: Could not process the query.';
            $this->session->set_userdata('feedback', $msg);
        }
    }

    /**
     * Edit project
     * @param type $id
     */
    public function edit_project($id = '') {

        if ($this->input->post()) {
            $data = $this->input->post();
            $p = new Project_Model();
            $p->where('id', $id);
            $p->update($data);

            $msg = 'Success: The project has been updated.';
            $this->session->set_userdata('feedback', $msg);
        }
    }

    /**
     * Delete project
     * @param type $id
     */
    public function delete_project($id = '') {

        if (empty($id))show_404;

        $p = new Project_Model();
        $found = $p->where('id', $id)->count();
        if (!empty($found)) {

            $p->where('id', $id)->get();

            if ($p->delete()) {
                $msg = 'Success: The project has been deleted.';
                $this->session->set_userdata('feedback', $msg);
            }
        }

        $this->index();
    }

}

/*------------------------------------------------------------------------------*/
/*                              End of the class                                */
/*------------------------------------------------------------------------------*/ 
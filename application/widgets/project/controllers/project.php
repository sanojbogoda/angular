<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Project extends MX_Controller {

    public function __construct() {
        parent::__construct();
        
        //Models
        $this->load->model('project_model');
    }

    /**
     * Default view
     */
    public function index() {

        $data['records'] = $this->project_model->find_all();
        $data['view'] = 'project_view';
        $data['page'] = 'home';
        $this->load->module('template');
        $this->template->common($data);
    }

}

/*------------------------------------------------------------------------------*/
/*                              End of the class                                */
/*------------------------------------------------------------------------------*/ 
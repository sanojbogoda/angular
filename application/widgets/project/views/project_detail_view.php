<div class="row">
    <div class="col-sm-12">

        <!--Breadcrumbs-->
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url().index_page().'project'?>">Projects</a></li>
            <li class="active">Project Details</li>
        </ol>
        
        <!--Messages-->
        <?php echo Modules::run('message');?>

        <form data-validate="parsley" method='POST' action='<?php echo base_url() . index_page() . 'project/project_modify/'.$this->uri->segment(3); ?>'>
            <section class="panel">
                <header class="panel-heading">
                    <span class="h4">Project Details</span>
                </header>
                <div class="panel-body">
<!--                    <p class="text-muted">Need support? please fill the fields below.</p>       -->

                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-6">
                            <label>Contract No</label> : <?php if(!empty($row))echo $row->contract_no;?>
                        </div>
                        <div class="col-sm-6">
                            <label>Project Name</label> : <?php if(!empty($row))echo $row->pro_name;?>
                        </div>
                    </div>
                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-12">
                            <label>Project Description</label> : <?php if(!empty($row))echo $row->pro_desc;?>
                            </textarea>
                        </div>
                    </div>
                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-6">
                            <label>Project Type</label> : <?php echo $row->type;?>
                        </div>
                        <div class="col-sm-6">&nbsp;</div>
                    </div>
                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-6">
                            <label>Start Date</label> : <?php if(!empty($row))echo $row->start_date;?>
                        </div>
                        <div class="col-sm-6">
                            <label>End Date</label> : <?php if(!empty($row))echo $row->end_date;?>
                        </div>
                    </div>
                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-6">
                            <label>Site Name</label> : <?php if(!empty($row))echo $row->site_name;?>
                        </div>
                        <div class="col-sm-6">
                            <label>Site Editress</label> : <?php if(!empty($row))echo $row->site_add;?>
                        </div>
                    </div>
                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-12">
                            <label>Site Description</label> : <?php if(!empty($row))echo $row->site_desc;?>
                        </div>
                    </div>
                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-6">
                            <label>Latitude</label> : <?php if(!empty($row))echo $row->lat;?>
                        </div>
                        <div class="col-sm-6">
                            <label>Longitude</label> : <?php if(!empty($row))echo $row->lon;?>
                        </div>
                    </div>
                </div>
            </section>
        </form>
    </div>

</div>



<script type="text/javascript">
    function init(){
        //init
    }     
</script>
<div class="row">
    <div class="col-sm-12">

        <!--Breadcrumbs-->
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url().index_page().'project'?>">Projects</a></li>
            <li class="active"><?php echo $title;?></li>
        </ol>
        
        <!--Messages-->
        <?php echo Modules::run('message');?>

        <form data-validate="parsley" method='POST' action='<?php echo base_url() . index_page() . 'project/project_modify/'.$this->uri->segment(3); ?>'>
            <section class="panel">
                <header class="panel-heading">
                    <span class="h4"><?php echo $title;?></span>
                </header>
                <div class="panel-body">
<!--                    <p class="text-muted">Need support? please fill the fields below.</p>       -->

                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-6">
                            <label>Contract No</label>
                            <input type="text" value='<?php if(!empty($row))echo $row->contract_no;?>' name='contract_no' class="form-control parsley-validated" placeholder="No" data-required="true"/>
                        </div>
                        <div class="col-sm-6">
                            <label>Project Name</label>
                            <input type="text" value='<?php if(!empty($row))echo $row->pro_name;?>' name='pro_name' class="form-control parsley-validated" placeholder="Enter name" data-required="true"/>
                        </div>
                    </div>
                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-12">
                            <label>Project Description</label>
                            <textarea type="text" name='pro_desc' class="form-control parsley-validated" placeholder="Enter desc" data-required="true">
                                <?php if(!empty($row))echo $row->pro_desc;?>
                            </textarea>
                        </div>
                    </div>
                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-6">
                            <label>Project Type</label>
                            <select name='type' class="form-control m-t parsley-validated"> 
                                <option <?php if(!empty($row))if($row->type=='HDB')echo 'selected="true"';?> value="HDB">HDB</option> 
                                <option <?php if(!empty($row))if($row->type=='Condominium')echo 'selected="true"';?> value="Condominium">Condominium</option> 
                                <option <?php if(!empty($row))if($row->type=='Private')echo 'selected="true"';?> value="Private">Private</option> 
                            </select>
                        </div>
                        <div class="col-sm-6">&nbsp;</div>
                    </div>
                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-6">
                            <label>Start Date</label>
                            <input readonly='true' type="text" value="<?php if(!empty($row))echo $row->start_date;?>" name='start_date' class="form-control parsley-validated datepicker" data-required="true"/>
                        </div>
                        <div class="col-sm-6">
                            <label>End Date</label>
                            <input readonly='true' type="text" value="<?php if(!empty($row))echo $row->end_date;?>" name='end_date' class="form-control parsley-validated datepicker" data-required="true"/>
                        </div>
                    </div>
                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-6">
                            <label>Site Name</label>
                            <input type="text" name='site_name' value="<?php if(!empty($row))echo $row->site_name;?>" class="form-control parsley-validated" placeholder="Name" data-required="true"/>
                        </div>
                        <div class="col-sm-6">
                            <label>Site Editress</label>
                            <input type="text" name='site_add' value="<?php if(!empty($row))echo $row->site_add;?>" class="form-control parsley-validated" placeholder="Data" data-required="true"/>
                        </div>
                    </div>
                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-12">
                            <label>Site Description</label>
                            <textarea type="text" name='site_desc' class="form-control parsley-validated" placeholder="Enter desc" data-required="true">
                            <?php if(!empty($row))echo $row->site_desc;?>
                            </textarea>
                        </div>
                    </div>
                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-6">
                            <label>Latitude</label>
                            <input type="text" name='lat' value="<?php if(!empty($row))echo $row->lat;?>" class="form-control parsley-validated" placeholder="lat" data-required="true"/>
                        </div>
                        <div class="col-sm-6">
                            <label>Longitude</label>
                            <input type="text" name='lon' value="<?php if(!empty($row))echo $row->lon;?>" class="form-control parsley-validated" placeholder="lon" data-required="true"/>
                        </div>
                    </div>
                </div>
                <footer class="panel-footer text-right bg-light lter">
                    <button type="submit" class="btn btn-success btn-s-xs">Submit</button>
                </footer>
            </section>
        </form>
    </div>

</div>



<script type="text/javascript">
    function init(){
        //init
    }     
</script>
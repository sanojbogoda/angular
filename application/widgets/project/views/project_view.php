<div class="row">
    <div class="col-md-12">

        <!--Breadcrumbs-->
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url().index_page().'project'?>">Projects</a></li>
            <li class="active"></li>
        </ol>
        
       <!--Messages-->
        <?php echo Modules::run('message');?>
        
        <section class="panel">
            
            <header class="panel-heading">
                <span class="h4">Projects</span>
                <a href="<?php echo base_url().index_page().'project/project_modify';?>" class="btn btn-success btn-xs pull-right"><i class="icon-plus text-white"></i> Add</a>
            </header>
            
            <?php if(count($records)>0) { ?>

            <div class="table-responsive">
                <table class="table table-striped b-t text-sm">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Project</th>
                            <th>Address</th>
                            <th>Start Date</th>
                            <th>End End</th>
                            <th  width="12%">Options</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($records as $row) { ?>
                        <tr>
                            <td><?php echo $row->contract_no; ?></td>
                            <td><?php echo $row->pro_name; ?></td>
                            <td><?php echo $row->site_add; ?></td>
                            <td><?php echo $row->start_date; ?></td>
                            <td><?php echo $row->end_date; ?></td>
                            <td>
                                <a href="<?php echo base_url().index_page().'project/project_details/'.$row->id;?>"><i class="icon-search text-success"></i></a>
                                <a href="<?php echo base_url().index_page().'project/project_modify/'.$row->id;?>"><i class="icon-pencil text-success"></i></a>
                                <a class="delete" href="<?php echo base_url().index_page().'project/delete_project/'.$row->id;?>"><i class="icon-remove text-danger"></i></a>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            
            <?php } else { echo RNA;} ?>

        </section>
 

    </div>
</div>

<script type="text/javascript">
    function init(){
        //init
    }     
</script>
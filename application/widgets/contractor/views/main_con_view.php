<div class="row">
    <div class="col-sm-12">

        <!--Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="active">Main-contractor</li>
            <li></li>
        </ol>
        
        <!--Messages-->
        <?php echo Modules::run('message');?>

        <form data-validate="parsley" method='POST' action='<?php echo base_url() . index_page() . 'project/project_modify/'.$this->uri->segment(3); ?>'>
            <section class="panel">
                <header class="panel-heading">
                    <span class="h4">Company Details</span>
                </header>
                <div class="panel-body">
<!--                    <p class="text-muted">Need support? please fill the fields below.</p>       -->

                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-6">
                            <label>Name</label> : <?php if(!empty($row))echo $row->name;?>
                        </div>
                        <div class="col-sm-6">
                            <label>Address</label> : <?php if(!empty($row))echo $row->address;?>
                        </div>
                    </div>
                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-12">
                            <label>Description</label> : <?php if(!empty($row))echo $row->description;?>
                            </textarea>
                        </div>
                    </div>
                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-6">
                            <label>Tel</label> : <?php echo $row->tel;?>
                        </div>
                        <div class="col-sm-6">
                            <label>Fax</label> : <?php if(!empty($row))echo $row->fax;?>
                        </div>
                    </div>
                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-6">
                            <label>Email</label> : <?php if(!empty($row))echo $row->email;?>
                        </div>
                        <div class="col-sm-6">
                            <label>Date Added</label> : <?php if(!empty($row))echo $row->date_added;?>
                        </div>
                    </div>
                </div>
            </section>
        </form>
    </div>

</div>



<script type="text/javascript">
    function init(){
        //init
    }     
</script>
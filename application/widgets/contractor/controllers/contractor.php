<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Contractor extends MX_Controller {

    public function __construct() {
        parent::__construct();
        
        //Models
        $this->load->model('contractor_model');
    }

    public function index() {

        $data['records'] = $this->contractor_model->where('type','Sub')->find_all();
        $data['view'] = 'sub_con_view';
        $data['page'] = 'home';
        $this->load->module('template');
        $this->template->common($data);
    }
    
    
    public function main() {

        $data['row'] = $this->contractor_model->find_by('type','Main');
        $data['view'] = 'main_con_view';
        $data['page'] = 'home';
        $this->load->module('template');
        $this->template->common($data);
    }
    
    

}

/*------------------------------------------------------------------------------*/
/*                              End of the class                                */
/*------------------------------------------------------------------------------*/ 
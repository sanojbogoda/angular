<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Test extends MX_Controller {

    public function __construct() {
        parent::__construct();

    }

    public function index() {
        $data['page'] = 'sample';
        $this->load->module('template');
        $this->template->emp($data);
    }

    function view1() {
        $this->load->view('one');
    }

    function view2() {
        $this->load->view('two');
    }

}

/*------------------------------------------------------------------------------*/
/*                              End of the class                                */
/*------------------------------------------------------------------------------*/ 
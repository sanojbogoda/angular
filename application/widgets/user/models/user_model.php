<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class User_Model extends MY_Model {

    protected $table = 'user';
    protected $key = 'id';
    protected $soft_deletes = false;
    protected $date_format = 'datetime';
    protected $set_created = true;
    protected $set_modified = false;

}

/*------------------------------------------------------------------------------*/
/*                              End of the class                                */
/*------------------------------------------------------------------------------*/ 
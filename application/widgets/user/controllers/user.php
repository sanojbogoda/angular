<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class User extends MX_Controller {

    public function __construct() {
        parent::__construct();
        
        $this->load->model('user_model');

    }

    /**
     * Default view
     */
    public function index() {

        $records = $this->user_model->find_all_by(array('contractor_id'=>1, 'role'=> 'staff'));
        
        
        $data['records'] = $records;
        $data['view'] = 'user_view';
        $data['page'] = 'project';
        $this->load->module('template');
        $this->template->common($data);

    }

 
}

/*------------------------------------------------------------------------------*/
/*                              End of the class                                */
/*------------------------------------------------------------------------------*/ 
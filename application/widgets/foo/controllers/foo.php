<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Foo extends MX_Controller {

    public function __construct() {
        parent::__construct();
        
    }

    public function fooview() {
        $this->load->view('foo_view');
    }
    
}
<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Message extends MX_Controller {

    public function index() {

        $feedback = $this->session->userdata('feedback');

        if (!empty($feedback)) {

            $arr = explode(':', $feedback);
            $data['type'] = $arr[0];
            $data['msg'] = $arr[1];
            $this->load->view('message_view', $data);

            $this->session->unset_userdata('feedback');
        }
    }

}

/*------------------------------------------------------------------------------*/
/*                              End of the class                                */
/*------------------------------------------------------------------------------*/ 

<?php if ($type == 'Success') { ?>

    <div class="alert alert-success"> 
        <button type="button" class="close" data-dismiss="alert">
            <i class="icon-remove"></i>
        </button> 
        <i class="icon-ok-sign"></i>
        <strong>Success! </strong> <?php echo $msg; ?> 
    </div>

<?php } else if ($type == 'Error') { ?>


    <div class="alert alert-danger"> 
        <button type="button" class="close" data-dismiss="alert">
            <i class="icon-remove"></i>
        </button> 
        <i class="icon-ok-sign"></i>
        <strong>Error! </strong> <?php echo $msg; ?> 
    </div>


<?php } ?>

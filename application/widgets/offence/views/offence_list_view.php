<div class="row">
    <div class="col-md-12">

        <!--Breadcrumbs-->
        <ol class="breadcrumb">
            <li  class="active">Offences</li>
            <li></li>
        </ol>
        
       <!--Messages-->
        <?php echo Modules::run('message');?>
        
        <section class="panel">
            
            <header class="panel-heading">
                <span class="h4">Offences</span>
            </header>
            
            <?php if(count($records)>0) { ?>

            <div class="table-responsive">
                <table class="table table-striped b-t text-sm">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Status</th>
                            <th>Message</th>
                            <th>Action</th>
                            <th>Contractor</th>
                            <th>Worker Name</th>
                            <th>Emp No.</th>
                            <th>NRIC/Fin</th>
                            <th>Data</th>
                            <th>Options</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($records as $row) { ?>
                        <tr>
                            <td><?php echo $row->id; ?></td>
                            <td><?php echo $row->status; ?></td>
                            <td><?php echo $row->message; ?></td>
                            <td><?php echo $row->action; ?></td>
                            <td><?php echo $row->name; ?></td>
                            <td><?php echo $row->worker_name; ?></td>                          
                            <td><?php echo $row->emp_id; ?></td>                          
                            <td><?php echo $row->nric_fin; ?></td>                          
                            <td><?php echo $row->date_added; ?></td>                          
                            <td>
                                <a href="<?php echo base_url().index_page().'offence/offence_details/'.$row->id;?>"><i class="icon-search text-success"></i></a>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            
            <?php } else { echo RNA;} ?>

        </section>
 

    </div>
</div>

<script type="text/javascript">
    function init(){
        //init
    }     
</script>
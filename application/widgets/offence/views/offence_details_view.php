<div class="row">
    <div class="col-sm-12">

        <!--Breadcrumbs-->
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . index_page() . 'offence' ?>">Offence</a></li>
            <li class="active">Offence Details</li>
        </ol>

        <!--Messages-->
        <?php echo Modules::run('message'); ?>

        <section class="panel">
            <header class="panel-heading">
                <span class="h4">Offence Details</span>
            </header>
            <div class="panel-body">

                <div class="form-group pull-in clearfix">
                    <div class="col-sm-6">
                        <label>Status</label> : <span class="label bg-warning"> <?php if (!empty($row)) echo $row->status; ?> </span>
                    </div>
                    <div class="col-sm-6">
                        <label>Action</label> : <span class="label bg-danger"> <?php if (!empty($row)) echo $row->action; ?> </span>
                    </div>
                </div>
                <div class="form-group pull-in clearfix">
                    <div class="col-sm-12">
                        <label>Message</label> : <?php if (!empty($row)) echo $row->message; ?>
                        </textarea>
                    </div>
                </div>
                <div class="form-group pull-in clearfix">
                    <div class="col-sm-6">
                        <label>Sub-Contractor</label> : <?php echo $row->name; ?>
                    </div>
                    <div class="col-sm-6">&nbsp;</div>
                </div>
                <div class="form-group pull-in clearfix">
                    <div class="col-sm-6">
                        <label>Worker</label> : <?php if (!empty($row)) echo $row->worker_name; ?>
                    </div>
                    <div class="col-sm-6">
                        <label>NRIC/Fin</label> : <?php if (!empty($row)) echo $row->nric_fin; ?>
                    </div>
                </div>
                <div class="form-group pull-in clearfix">
                    <div class="col-sm-6">
                        <label>Acknowledged</label> : <?php if (!empty($row)) echo $row->ack; ?>
                    </div>
                    <div class="col-sm-6">
                        <label>Signature</label> : <?php if (!empty($row)) echo $row->signature; ?>
                    </div>
                </div>

                <!--Offence types-->
                <?php if (count($types) > 0) { ?>
                    <?php foreach ($types as $type) { ?>
                            <div class="alert alert-danger">
                                <i class="icon-ban-circle"></i> <?php echo $type->off_name; ?>
                            </div>
                    <?php } ?>
                <?php } ?>

                <!-- Gallery -->
                <div class="form-group pull-in clearfix">
                    <div class="col-sm-3">
                        <img src="<?php echo base_url(); ?>" alt="" width="100%"/>
                    </div>
                </div>

            </div>


        </section>

    </div>

</div>



<script type="text/javascript">
    function init() {
        //init
    }
</script>
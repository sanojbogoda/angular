<div class="row">
    <div class="col-md-12">


        <!--Messages-->
        <?php echo Modules::run('message'); ?>

        <section class="panel">

            <!--Breadcrumbs-->
            <ol class="breadcrumb">
                <li  class="active">Offence Types</li>
                <li></li>
            </ol>

            <header class="panel-heading">
                <span class="h4">Offence Types</span>
                <a href="<?php echo base_url() . index_page() . 'project/project_modify'; ?>" class="btn btn-success btn-xs pull-right"><i class="icon-plus text-white"></i> Add</a>
            </header>

            <?php if (count($records) > 0) { ?>

                <div class="table-responsive">
                    <table class="table table-striped b-t text-sm">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th width="35%">Name</th>
                                <th>Description</th>
                                <th width="12%">Options</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($records as $row) { ?>
                                <tr>
                                    <td><?php echo $row->id; ?></td>
                                    <td><?php echo $row->off_name; ?></td>
                                    <td><?php echo $row->off_desc; ?></td>
                                    <td>
                                        <a href="<?php echo base_url() . index_page() . 'project/project_details/' . $row->id; ?>"><i class="icon-search text-success"></i></a>
                                        <a href="<?php echo base_url() . index_page() . 'project/project_modify/' . $row->id; ?>"><i class="icon-pencil text-success"></i></a>
                                        <a class="delete" href="<?php echo base_url() . index_page() . 'project/delete_project/' . $row->id; ?>"><i class="icon-remove text-danger"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>

            <?php } else {
                echo RNA;
            } ?>

        </section>


    </div>
</div>

<script type="text/javascript">
    function init() {
        //init
    }
</script>
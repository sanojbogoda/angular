<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Offence extends MX_Controller {

    public function __construct() {
        parent::__construct();
        
        //Models
        $this->load->model('offence_type_model');
        $this->load->model('offence_model');
        $this->load->model('worker_offence_model');
    }

    public function index() {
        
        $data['records'] = $this->offence_model->get_offences();
        $data['view'] = 'offence_list_view';
        $data['page'] = 'offence';
        $this->load->module('template');
        $this->template->common($data);

    }
    
    
    public function type() {

        $data['records'] = $this->offence_type_model->find_all();
        $data['view'] = 'offence_view';
        $data['page'] = 'offence';
        $this->load->module('template');
        $this->template->common($data);
    }
    
    
    
    public function offence_details($id = 0) {
        
        if (empty($id))show_404();

        $data['row'] = $this->offence_model->get_offence_details($id);
        
        $data['types'] = $this->worker_offence_model->get_types($id);
        
        $data['view'] = 'offence_details_view';
        $data['page'] = 'offence';
        $this->load->module('template');
        $this->template->common($data);
    }
    
    
    
    

}

/*------------------------------------------------------------------------------*/
/*                              End of the class                                */
/*------------------------------------------------------------------------------*/ 
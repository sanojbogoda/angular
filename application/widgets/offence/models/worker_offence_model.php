<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Worker_Offence_Model extends MY_Model {

    protected $table = 'worker_offence';
    protected $key = 'id';
    protected $soft_deletes = false;
    protected $date_format = 'datetime';
    protected $set_created = true;
    protected $set_modified = false;
    
    
    public function get_types($id = 0) {
        $this->db->join('offence_type', 'offence_type.id = worker_offence.off_type_id', 'inner');
        return $this->find_all_by('off_id',$id);
    }

    

}

/*------------------------------------------------------------------------------*/
/*                              End of the class                                */
/*------------------------------------------------------------------------------*/ 
<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Offence_Model extends MY_Model {

    protected $table = 'offence';
    protected $key = 'id';
    protected $soft_deletes = false;
    protected $date_format = 'datetime';
    protected $set_created = true;
    protected $set_modified = false;
    
    
    public function get_offences() {
        $this->db->join('contractor', 'offence.contractor_id = contractor.id', 'inner');
        return $this->find_all();
    }
    
    
    public function get_offence_details($id = 0) {
        $this->db->join('contractor', 'offence.contractor_id = contractor.id', 'inner');
        return $this->find($id);
    }
    

}

/*------------------------------------------------------------------------------*/
/*                              End of the class                                */
/*------------------------------------------------------------------------------*/ 
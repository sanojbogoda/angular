<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Profile extends MX_Controller {

    public function __construct() {
        parent::__construct();
        
        //Models

    }

    public function index() {
        
        //$data['records'] = $this->offence_model->find_all();
        $data['view'] = 'profile_view';
        $data['page'] = 'profile';
        $this->load->module('template');
        $this->template->common($data);

    }


}

/*------------------------------------------------------------------------------*/
/*                              End of the class                                */
/*------------------------------------------------------------------------------*/ 
<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Offence_Model extends MY_Model {

    protected $table = 'offence';
    protected $key = 'id';
    protected $soft_deletes = false;
    protected $date_format = 'datetime';
    protected $set_created = true;
    protected $set_modified = false;

}

/*------------------------------------------------------------------------------*/
/*                              End of the class                                */
/*------------------------------------------------------------------------------*/ 
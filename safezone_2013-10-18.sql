# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: localhost (MySQL 5.5.29)
# Database: safezone
# Generation Time: 2013-10-18 09:58:53 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table contractor
# ------------------------------------------------------------

DROP TABLE IF EXISTS `contractor`;

CREATE TABLE `contractor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `address` varchar(500) DEFAULT NULL,
  `tel` int(11) DEFAULT NULL,
  `fax` int(11) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `date_added` date DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL COMMENT 'type = main/sub',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `contractor` WRITE;
/*!40000 ALTER TABLE `contractor` DISABLE KEYS */;

INSERT INTO `contractor` (`id`, `name`, `description`, `address`, `tel`, `fax`, `email`, `date_added`, `type`)
VALUES
	(1,'KeyLim','Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat','Office 33\n27 Colmore Row\nBirmingham\nEngland\nB3 2EW\n ',98567765,66567765,'sample@gmail.com','2013-12-12','Main'),
	(2,'Painters','quat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat','Studio 103\nThe Business Centre\n61 Wellfield Road\nRoath\nCardiff\nCF24 3DG',98567765,66567765,'sample@gmail.com','2013-12-12','Sub'),
	(3,'FloorStylers','m, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat','Department 98\n44-46 Morningside Road\nEdinburgh\nScotland\nEH10 4BF\n ',98567765,66567765,'sample@gmail.com','2013-12-12','Sub');

/*!40000 ALTER TABLE `contractor` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table fine
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fine`;

CREATE TABLE `fine` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(45) DEFAULT NULL,
  `amount` text,
  `date_settled` date DEFAULT NULL,
  `off_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_fine_offence1_idx` (`off_id`),
  CONSTRAINT `fk_fine_offence1` FOREIGN KEY (`off_id`) REFERENCES `offence` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `fine` WRITE;
/*!40000 ALTER TABLE `fine` DISABLE KEYS */;

INSERT INTO `fine` (`id`, `status`, `amount`, `date_settled`, `off_id`)
VALUES
	(1,'Pending','100.00','2013-12-12',2);

/*!40000 ALTER TABLE `fine` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table offence
# ------------------------------------------------------------

DROP TABLE IF EXISTS `offence`;

CREATE TABLE `offence` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(45) DEFAULT NULL,
  `message` varchar(500) DEFAULT NULL,
  `action` varchar(100) DEFAULT NULL COMMENT 'Warning/Fine/Ban',
  `contractor_id` int(11) DEFAULT NULL,
  `date_added` date DEFAULT NULL,
  `worker_name` varchar(100) DEFAULT NULL,
  `emp_id` varchar(100) DEFAULT NULL,
  `nric_fin` varchar(100) DEFAULT NULL,
  `ack` varchar(50) DEFAULT NULL,
  `signature` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_offence_contractor1_idx` (`contractor_id`),
  CONSTRAINT `fk_offence_contractor1` FOREIGN KEY (`contractor_id`) REFERENCES `contractor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `offence` WRITE;
/*!40000 ALTER TABLE `offence` DISABLE KEYS */;

INSERT INTO `offence` (`id`, `status`, `message`, `action`, `contractor_id`, `date_added`, `worker_name`, `emp_id`, `nric_fin`, `ack`, `signature`)
VALUES
	(1,'Pending','@Josh Long Vestibulum ullamcorper sodales nisi nec adipiscing elit. Morbi id neque quam. Aliquam sollicitudin venenatis @Josh Long Vestibulum ullamcorper sodales nisi nec adipiscing elit. Morbi id neque quam. Aliquam sollicitudin venenatis','Warning',2,'2013-12-12','SOme guye89','HD98988','G345345','Yes','sig1.jpg'),
	(2,'Completed','Another optional message','Fine',3,'2013-09-02','Another guy','GFH(8768','G*76576576','No',NULL),
	(3,'Pending','Tesy dfgkjdsnfpj ouyfiy','Ban',3,'2013-05-07','FSIDUA piudshf oisdhf oyuo','H978987','G87687557','Yes','sig2.jpg');

/*!40000 ALTER TABLE `offence` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table offence_photo
# ------------------------------------------------------------

DROP TABLE IF EXISTS `offence_photo`;

CREATE TABLE `offence_photo` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `off_id` int(11) DEFAULT NULL,
  `photo_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `offence_photo` WRITE;
/*!40000 ALTER TABLE `offence_photo` DISABLE KEYS */;

INSERT INTO `offence_photo` (`id`, `off_id`, `photo_id`)
VALUES
	(1,1,3),
	(2,1,4),
	(3,1,5),
	(4,1,6);

/*!40000 ALTER TABLE `offence_photo` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table offence_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `offence_type`;

CREATE TABLE `offence_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `off_name` varchar(250) DEFAULT NULL,
  `off_desc` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `offence_type` WRITE;
/*!40000 ALTER TABLE `offence_type` DISABLE KEYS */;

INSERT INTO `offence_type` (`id`, `off_name`, `off_desc`)
VALUES
	(1,'Working at restricted hights','Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.'),
	(2,'Imporper attire','Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.'),
	(3,'Unauthorized access','Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.'),
	(4,'Unauthorized usage of equipments','Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.'),
	(5,'Misuse of tools','Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.'),
	(6,'Misuse of resources','Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.'),
	(7,'Unauthorized usage of vehicles','Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.'),
	(8,'Adoid safety advice','Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.'),
	(9,'Ignore safety precautions','Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.'),
	(10,'Others','Some other stuff');

/*!40000 ALTER TABLE `offence_type` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table photo
# ------------------------------------------------------------

DROP TABLE IF EXISTS `photo`;

CREATE TABLE `photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `photo` varchar(500) DEFAULT NULL,
  `date_added` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `photo` WRITE;
/*!40000 ALTER TABLE `photo` DISABLE KEYS */;

INSERT INTO `photo` (`id`, `photo`, `date_added`)
VALUES
	(3,'off1.jpeg','2013-12-03'),
	(4,'off2.jpeg','2013-12-03'),
	(5,'off3.jpeg','2013-12-03'),
	(6,'off4.jpeg','2013-12-03');

/*!40000 ALTER TABLE `photo` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table project
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project`;

CREATE TABLE `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contract_no` varchar(45) DEFAULT NULL,
  `pro_name` varchar(250) DEFAULT NULL,
  `pro_desc` varchar(250) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `site_name` varchar(250) DEFAULT NULL,
  `site_add` varchar(500) DEFAULT NULL,
  `site_desc` varchar(500) DEFAULT NULL,
  `lat` decimal(11,11) DEFAULT NULL,
  `lon` decimal(11,11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;

INSERT INTO `project` (`id`, `contract_no`, `pro_name`, `pro_desc`, `type`, `start_date`, `end_date`, `site_name`, `site_add`, `site_desc`, `lat`, `lon`)
VALUES
	(1,'NB2','Garment Project','sofuyg suydgfo udygo uyg oug ouyg ouygouy                                                            ','Private','2013-10-21','2013-10-30','ouyg ouygouyg','ouy','go                                                        ouygo',0.99999999999,0.99999999999),
	(2,'ASN-234','Super Market Building','ouygfou yfgoseufyg sdouygouy                                                            ','Private','2013-11-19','2013-11-29','ouyg ouygo uygouyg ouyg','ouygouygouygouyg','                                                        ouygouygouygouygo',0.99999999999,0.99999999999);

/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(250) DEFAULT NULL,
  `last_name` varchar(250) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `password` varchar(250) DEFAULT NULL,
  `designation` varchar(45) DEFAULT NULL,
  `date_added` date DEFAULT NULL,
  `role` varchar(45) DEFAULT NULL,
  `contractor_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_contractor_idx` (`contractor_id`),
  CONSTRAINT `fk_user_contractor` FOREIGN KEY (`contractor_id`) REFERENCES `contractor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;

INSERT INTO `user` (`id`, `first_name`, `last_name`, `email`, `password`, `designation`, `date_added`, `role`, `contractor_id`)
VALUES
	(1,'Admin','','sombody@yahoo.com.uk',NULL,'Assitant','0000-00-00','Admin',1),
	(2,'John','Smith','sombody@yahoo.com.uk',NULL,'Assitant','0000-00-00','Staff',2),
	(3,'Andrew','Stew','sombody@yahoo.com.uk',NULL,'Assitant','0000-00-00','Staff',3),
	(4,'Fedric','Doe','sombody@yahoo.com.uk',NULL,'Assitant','0000-00-00','Staff',1),
	(5,'William','Mountainfox','sombody@yahoo.com.uk',NULL,'Assitant','0000-00-00','Staff',1);

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_project_role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_project_role`;

CREATE TABLE `user_project_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(100) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_project_role_user1_idx` (`user_id`),
  KEY `fk_user_project_role_project1_idx` (`project_id`),
  CONSTRAINT `fk_user_project_role_project1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_project_role_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `user_project_role` WRITE;
/*!40000 ALTER TABLE `user_project_role` DISABLE KEYS */;

INSERT INTO `user_project_role` (`id`, `role`, `user_id`, `project_id`)
VALUES
	(1,'Safety Officer',2,1),
	(2,'Project Manager',3,2);

/*!40000 ALTER TABLE `user_project_role` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table woker_offence_other
# ------------------------------------------------------------

DROP TABLE IF EXISTS `woker_offence_other`;

CREATE TABLE `woker_offence_other` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` varchar(500) DEFAULT NULL,
  `off_other_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_woker_offence_other_worker_offence1_idx` (`off_other_id`),
  CONSTRAINT `fk_woker_offence_other_worker_offence1` FOREIGN KEY (`off_other_id`) REFERENCES `worker_offence` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `woker_offence_other` WRITE;
/*!40000 ALTER TABLE `woker_offence_other` DISABLE KEYS */;

INSERT INTO `woker_offence_other` (`id`, `message`, `off_other_id`)
VALUES
	(1,'@Josh Long Vestibulum ullamcorper sodales nisi nec adipiscing elit. Morbi id neque quam. Aliquam sollicitudin venenatis @Josh Long Vestibulum ullamcorper sodales nisi nec adipiscing elit. Morbi id neque quam. Aliquam sollicitudin venenatis @Josh Long Vestibulum ullamcorper sodales nisi nec adipiscing elit. Morbi id neque quam. Aliquam sollicitudin venenatis',8);

/*!40000 ALTER TABLE `woker_offence_other` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table worker_offence
# ------------------------------------------------------------

DROP TABLE IF EXISTS `worker_offence`;

CREATE TABLE `worker_offence` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `off_id` int(11) DEFAULT NULL,
  `off_type_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `worker_offence` WRITE;
/*!40000 ALTER TABLE `worker_offence` DISABLE KEYS */;

INSERT INTO `worker_offence` (`id`, `off_id`, `off_type_id`)
VALUES
	(1,1,1),
	(2,1,2),
	(3,1,3),
	(4,2,4),
	(5,2,5),
	(6,3,5),
	(7,3,6),
	(8,3,10);

/*!40000 ALTER TABLE `worker_offence` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
